<?php
require '../api/functions.php';
session_issruning();
if (isloged()==0){
    header('Location: ../login.php');
    die();
}
?>
<?php include '../i18n.class.php'; $i18n = new i18n('../lang/lang_{LANGUAGE}.ini'); $i18n->init(); $title=L::mbox_inbox; include "../includes/header.php";?>
        <div style='color: white; background: #30303c; padding: 1%;'>
            <h1 style='margin: 0px;'>Mail Box <?php if(isset($_GET['box'])==true){echo $_GET['box'];}?></h1>
            <hr>
        <?php
        if (isset($_GET['info'])){
            echo "<text>".str_replace('_',' ',$_GET['info'])."</text>";
			echo '<br>';
        }
        function scan_dir($dir) {
            $ignored = array('.', '..', '.htaccess','mailb.php','getmail.php');
            $files = array();
            foreach (scandir($dir) as $file) {
                if (in_array($file, $ignored)) continue;
                    $files[$file] = filemtime($dir . '/' . $file);
                }
                arsort($files);
                $files = array_keys($files);
                return ($files) ? $files : false;
        }
        if (isloged()==1){
            if (isset($_GET['box'])){
                if (is_dir(preg_split('/@/',$_SESSION['m_user'])[0].'/'.$_GET['box']) and strpos($_GET['box'],'/')==false and strpos($_GET['box'],'..')==false or strpos($_GET['box'],'\\')==false){
                    $dirs=scan_dir(getcwd().'/'.preg_split('/@/',$_SESSION['m_user'])[0].'/'.$_GET['box']);
                    if (($dirs==false)==false){
                        foreach ($dirs as $i){
                            $date='';
                            include preg_split('/@/',$_SESSION['m_user'])[0].'/mails/'.$i;
                            echo '<span class="mail"><a href="getmail.php?box='.$_GET['box'].'&id='.$i.'">'.$sender.'</a> <text>'.str_replace('-','/',$date).'</text> <a href="getmail.php?delthem='.$i.'">Delete</a></span><br>';
                        }
                    }
                }
            }
            else{
                $dirs=scan_dir(getcwd().'/'.preg_split('/@/',$_SESSION['m_user'])[0].'/mails/');
                if (($dirs==false)==false){
                    foreach ($dirs as $i){
                        $date='';
                        include preg_split('/@/',$_SESSION['m_user'])[0].'/mails/'.$i;
                        echo '<span class="mail"><a href="getmail.php?id='.$i.'">'.$sender.'</a> <div class="go"><text>'.str_replace('-','/',$date).'</text> <a href="getmail.php?delthem='.$i.'">Delete</a></div></span><br>';
                    }
                }
                else{
                    echo '<text>'.L::mbox_dhave.'<br></text>';
                }
            }
        }
        else{
            http_response_code(404);
        }

        ?>
	<p><?php echo L::mbox_note." UTC".$instance_utc_time ?></p>
    <a href='/send_m.php'><button><?php echo L::mbox_send ?></button></a> <a href='/ch_p.php'><button><?php echo L::mbox_change ?></button></a>
<?php include "includes/footer.php" ?>