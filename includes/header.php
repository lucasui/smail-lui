<!DOCTYPE html>
<html lang="<?php echo L::index_code ?>" prefix="og: https://ogp.me/ns#">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <title><?php echo $title ?> | SecureMail</title>
        <meta property="og:title" content="SMail">
        <meta property="og:locale" content="<?php echo L::index_code ?>">
        <meta property="og:description" content="<?php echo L::index_about_c ?>">
        <link rel='stylesheet' type='text/css' href='/css/index.css'>
        <style type='text/css'>
          input{
              display: block;
          }
        </style>
        <?php if($instance_image_favicon!=false){echo "<link rel='icon' href='$instance_image_favicon' type='image/x-icon'>";} ?>
    </head>
    <body style='margin: 0px;'>
        <header style="color:white;background:#30303c;margin:50px 0 0 0;text-align:center;">
            <a href="/" style="color:white"><h2 id="logo" style='margin:0px;'><?php if($instance_image_logo==false){echo $instance_name;}else{echo "<img src='$instance_image_logo' alt='Logo' width='70px' height='auto'>";} ?></h2></a>
            Una instancia de SMail
            <!--<p style='margin:0px;'><?php echo L::index_welcome." ".$instance_name; ?></p>-->
            <nav style="color:white;background:#30303c;text-align:center"><a href="/new_user.php"><button><?php echo L::reg_submit; ?></button></a> <a href="/login.php"><button><?php echo L::login_postlogin_submit; ?></button></a> <a href="/mailbox/mailb.php"><button><?php echo L::mbox_inbox ?></button></a> <a href="/send_m.php"><button><?php echo L::send_send ?></button></a></nav>
        </header><br>